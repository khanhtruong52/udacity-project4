# Hosting a Full-Stack Application

### Overview

The purpose of this project is to learn how to setup a production environment for web application using AWS and CircleCI

Working Frontend URL:
> http://truongnk3-pj4-bucket.s3-website-us-east-1.amazonaws.com/

### CircleCI configuration
> Path: .circleci/config.yml

Config file for circleCI for CI/CD

## Documents

> Path: ./documents

1. Application_dependencies.md - Important dependencies for FE and BE.
1. Infrastructure_description.md - AWS Infrastucture used for this app.
1. Pipeline_description.md - Description for steps of pipelines

There are no Unit test on the back-end

## Screenshots:
> Path: ./screenshots

Includes screenshots of S3, Beanstalk, RDS, CircleCI

***Important screenshot:***

- Overview infrastucture: 
<img src="./screenshots/aws.jpg" alt="aws" />
- CircleCI last successful build: 
<img src="./screenshots/circleCI/circleCI_dashboard.png" alt="cirleCI" />
- Elastic Beanstalk Environment Variables:
<img src="./screenshots/beanstalk/environment_variables_in_aws_eb.png" alt="eb env vars" />

