# AWS Infrastructure

<img src="../screenshots/aws.jpg" alt="aws" />

## Components

### RDS Postgres Database
> AWS Postgres Database

<img src="../screenshots/rds/rds_postgres_database.png" alt="rds" />

### Elastic Beanstalk
> Deploy and manage applications in the AWS Cloud to host back-end application.

<img src="../screenshots/beanstalk/aws_beanstalk.png" alt="beanstalk" />

### S3
> A simple storage holding static files to host front-end application.

<img src="../screenshots/s3/aws_s3_properties.png" alt="s3" />

## Workflow
- User opens application via endpoit that s3 provided.
- Frontend app will interact with AWS Beanstalk which is Backend app.
- Beanstalk will manipulate data in RDS and return it to FE app.