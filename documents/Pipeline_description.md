# CircleCI Pipeline Description

## Overview 
<img src="../screenshots/circleCI/circleCI_overview.jpg" alt="circleCI overview" />
 

## Steps

### **Build**
> The purpose of run step is create a built bundle to deploy. It usually will install packages, check code's format, build code.  

<img src="../screenshots/circleCI/build/build_phase.png" alt="circleCI build" />


### **Hold**
> Hold step is to wait for an approval for important step such as deploy

<img src="../screenshots/circleCI/hold/hold_phase.png" alt="circleCI hold" />

### **Deploy**
> Deploy app to production environment. For this lession is AWS 

<img src="../screenshots/circleCI/deploy/deploy_phase.png" alt="circleCI deploy" />