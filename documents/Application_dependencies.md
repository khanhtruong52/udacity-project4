# Important Dependencies

## Front-end (S3)
- angular
- typescript
- tslint
- jasmine & karma 
- ionic

## Back-end (Beanstalk)
- express
- typescript
- aws-sdk
- pg
- eslint